const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const entryRoutes = express.Router();
const PORT = 4000;

let Entry = require('./entry.model');
app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/entry',{ useNewUrlParser: true });
const connection = mongoose.connection;

connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})

entryRoutes.route('/').get(function(req, res) {
    Entry.find(function(err, entry) {
        if (err) {
            console.log(err);
        } else {
            res.json(entry);
        }
    }).select(['_id','location_name','situation_details','location_rating','service_rating']);
});

entryRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    Entry.findById(id, function(err, entry) {
        res.json(entry);
    }).select(['_id','location_name','situation_details','location_rating','service_rating']);

});

app.get('/search', function(req, res) {
    let term = req.query;
    console.log(term);
    var searchParams = term['q'].toUpperCase().split(' ');
    var reg = new RegExp([".*", searchParams, ".*"].join(""), "i");    
    Entry.find( { $text: { $search: reg }} , function(err, entry) {
        res.json(entry);
    }).select(['_id','location_name','situation_details','location_rating','service_rating']);

});

entryRoutes.route('/add').post(function(req, res) {
    let entry = new Entry(req.body);
    console.log(entry)
    entry.save()
        .then(entry => {
            res.status(200).json({'entry': 'entry added successfully'});
        })
        .catch(err => {
            res.status(400).send('adding new entry failed');
        });
});

app.use('/entry', entryRoutes);

app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});
