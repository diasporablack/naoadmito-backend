const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Entry = new Schema({
    date: {
        type: Date
    },
    falta_quali: {
        type: Boolean
    },
    violencia_verbal: {
        type: Boolean
    },
    violencia_fisica: {
        type: Boolean
    },
    recep_preconc: {
        type: Boolean
    },
    racismo: {
        type: Boolean
    },
    homofobia: {
        type: Boolean
    },
    assedio_sex: {
        type: Boolean
    },
    discrim_outras: {
        type: Boolean
    },
    ineficiencia_encamin: {
        type: Boolean
    },
    explo_sex: {
        type: Boolean
    },
    tipo_outro: {
        type: Boolean
    },
    vivenciado_teceiros: {
        type: Boolean
    },
    vivenciado_voce: {
        type: Boolean
    },
    vivenciado_familia: {
        type: Boolean
    },
    vivenciado_amigos: {
        type: Boolean
    },
    autor_func: {
        type: Boolean
    },
    autor_terceiro: {
        type: Boolean
    },
    autor_client: {
        type: Boolean
    },
    autor_outro: {
        type: Boolean
    },
    name:  {
        type: String
    },
    age:  {
        type: String
    },
    document_id:  {
        type: String
    },
    email:  {
        type: String
    },
    phone: {
        type: String
    },
    gender:  {
        type: String
    },
    race:  {
        type: String
    },
    country:  {
        type: String
    },
    service_rating:  {
        type: String
    },
    is_it_a_crime:  {
        type: String
    },
    is_it_a_crime_details: {
        type: String
    },
    location_rating:  {
        type: String
    },
    will_return:  {
        type: String
    },
    situation_details:  {
        type: String
    },
    location_name:  {
        type: String
    },
    location_address: {
        type: String
    },
});
Entry.index({location_name: 'text', situation_details: 'text'});

module.exports = mongoose.model('Entry', Entry);
